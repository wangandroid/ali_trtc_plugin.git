import 'dart:async';

import 'package:flutter/services.dart';

class AliTrtcPlugin {
  static const MethodChannel _channel = const MethodChannel('ali_trtc_plugin');

  static String _appid = '';
  static String _appkey = '';

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  /// @param appid 应用id
  /// @param appkey  应用的key
  static void setAppidAndAppKey(String appID, String appKey) {
    _appid = appID;
    _appkey = appKey;
  }

  /// 创建房间
  static void createAudioAndVideoRoomChannel(String userName, String channel) {
    Map map = {
      "userName": userName,
      "channel": channel,
      "appID": _appid,
      'appKey': _appkey
    };
    _channel.invokeMethod('createAudioAndVideoRoomChannel', map);
  }

  ///加入房间
  static void joinAudioAndVideoRoom(String userName, String channel) {
    Map map = {
      "userName": userName,
      "channel": channel,
      "appID": _appid,
      'appKey': _appkey
    };
    _channel.invokeMethod('joinAudioAndVideoRoom', map);
  }

  ///结束房间
  static void disposeRoom(){
    _channel.invokeMethod('roomDestroy');
  }
}
