package trtc.flutter.com.ali_trtc_plugin

import alitrtcflutte.AliTrTcAction.*
import alitrtcflutte.sophon.utils.SharedPreferenceUtils
import alitrtcflutte.sophon.utils.SpHistoryUtils
import alitrtcflutte.sophon.utils.StringUtil
import android.content.Context
import androidx.annotation.NonNull
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter


/** Ali音视频 */
class AliTrtcPlugin : FlutterPlugin, MethodCallHandler {
    private lateinit var channel: MethodChannel
    private lateinit var context: Context
    private var mMyReceiver: MyReceiver? = null
    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "ali_trtc_plugin")
        channel.setMethodCallHandler(this)
        context = flutterPluginBinding.applicationContext;
        mMyReceiver = MyReceiver()
        mMyReceiver!!.setReceiver(channel)
        val lIntentFilter = IntentFilter("ali.flutter.call")
        context.registerReceiver(mMyReceiver, lIntentFilter)

    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        if (call.method == "AliNotice") {


        } else if (call.method == "createAudioAndVideoRoomChannel") {
            val userName: String? = call.argument<String>("userName");
            val channel: String? = call.argument<String>("channel");
            val appID: String? = call.argument<String>("appID");
            val appKey: String? = call.argument<String>("appKey");
            SharedPreferenceUtils.setAppId(context, appID);
            SharedPreferenceUtils.setAppKey(context, appKey);
            createChannel(userName.toString(), channel.toString());
        } else if (call.method == "joinAudioAndVideoRoom") {
            val userName: String? = call.argument<String>("userName");
            val channel: String? = call.argument<String>("channel");
            val appID: String? = call.argument<String>("appID");
            val appKey: String? = call.argument<String>("appKey");
            SharedPreferenceUtils.setAppId(context, appID);
            SharedPreferenceUtils.setAppKey(context, appKey);
            joinChannel(userName.toString(), channel.toString());
        } else if (call.method == "roomDestroy") {
            context.unregisterReceiver(mMyReceiver);
        } else {
            result.notImplemented()
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }


    /**
     * 创建会议
     * @author wyz
     */
    fun createChannel(userName: String, roomID: String) {
        StringUtil.clipChannelId(roomID, context)
        //保存会议码
        SpHistoryUtils.saveSearchHistory(context, roomID)
        //获取会议码，进入会议
        startVideoCallActivity(context, roomID, userName)
    }


    /**
     * 加入会议
     *@author wyz
     */
    fun joinChannel(userName: String, channelId: String) {
        //保存会议码
        SpHistoryUtils.saveSearchHistory(context, channelId)
        startVideoCallActivity(context, channelId, userName)
    }

    /**
     * 会议监听
     */
    class MyReceiver : BroadcastReceiver() {
        private lateinit var Mychannel: MethodChannel
        fun setReceiver(chanel: MethodChannel) {
            Mychannel = chanel
        }

        override fun onReceive(context: Context, intent: Intent) {
            val resultMap: MutableMap<String, Any> = HashMap()
            resultMap["code"] = 1001
            Mychannel.invokeMethod("aliTrcDispose", resultMap);
        }
    }

}
