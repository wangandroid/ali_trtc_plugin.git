#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint ali_trtc_plugin.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'ali_trtc_plugin'
  s.version          = '0.0.1'
  s.summary          = 'A new Flutter project.'
  s.description      = <<-DESC
A new Flutter project.
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.resources = ['Classes/RTCBeaconTower/Resources/*.bundle','Classes/RTCCommonView/Resources/*.bundle']
  s.dependency 'Flutter'
  s.dependency 'AFNetworking'
  s.dependency 'MBProgressHUD'
  s.dependency 'Masonry'
  s.dependency 'ReactiveObjC'
  s.dependency 'AliRTCSdk', '1.17.53'
  s.platform = :ios, '9.0'

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
end
