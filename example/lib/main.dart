import 'package:flutter/material.dart';
import 'dart:async';

import 'package:ali_trtc_plugin/ali_trtc_plugin.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = '412212193@qq.com';
  static const methodChannel = const MethodChannel('ali_trtc_plugin');
  @override
  void initState() {
    super.initState();
    initPlatformState();
    AliTrtcPlugin.setAppidAndAppKey(
        'e4p43wcg', '835752f5a0c91f40241552cc8f65db90');
    nativeMessageListener();
  }


  Future<dynamic> nativeMessageListener() async {
    methodChannel.setMethodCallHandler((resultCall) {
      MethodCall call = resultCall;
      Map arguments = call.arguments;
      int code = arguments["code"];
      setState(() {
        _platformVersion=code.toString();
      });
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
     String platformVersion;
     // Platform messages may fail, so we use a try/catch PlatformException.
     try {
       platformVersion = await AliTrtcPlugin.platformVersion;
     } on PlatformException {
       platformVersion = 'Failed to get platform version.';
     }
     // If the widget was removed from the tree while the asynchronous platform
     // message was in flight, we want to discard the reply rather than calling
     // setState to update our non-existent appearance.
     if (!mounted) return;
     setState(() {
       _platformVersion = platformVersion;
     });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('阿里音视频Flutter'),
        ),
        body: Center(
          child: GestureDetector(
            child: Text('Running on: $_platformVersion\n'),
            onTap: () {
              AliTrtcPlugin.joinAudioAndVideoRoom(
                  '123456789', '123456');
            },
          ),
        ),
      ),
    );
  }
}
